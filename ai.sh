#!/bin/bash

NOW="$(date +"%Y-%m-%d_%H:%M:%S")"


# Skip apt-get update if its not a fresh install
if [ "$1" = 'rocket' ] && [ "$2" = '--update' ]; then

echo -e "\033[32m
Doing and update..." &&

echo -e "\033[0m"

echo "Wont be too long...
"

#dpkg -s apache2 2>/dev/null >/dev/null || sudo apt-get -y install apache2 &&
#dpkg -s mc sudo php5 libapache2-mod-php5 php5-mysql mysql-client-5.5 php-pear php5-curl php5-mcrypt php5-gd php5-fpm unzip 2>/dev/null >/dev/null || sudo apt-get -y install mc sudo php5 libapache2-mod-php5 php5-mysql mysql-client-5.5 php-pear php5-curl php5-mcrypt php5-gd php5-fpm unzip
#todo Need to to a sanity check if the required packages are installed. I should create a required packages list as a seperate file and can use that and keep it maintained.




else

# If first time upgrade everything

echo -e "\033[32m
Doing a fresh install and updating dependencies..." &&

echo -e "\033[0m"

echo "Gimme a sec...
"

#sudo apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && apt-get install mc sudo php5 libapache2-mod-php5 php5-mysql mysql-client-5.5 php-pear php5-curl php5-mcrypt php5-gd php5-fpm -y
#todo Refactor this also see above
#Enable rewrite module
a2enmod rewrite ssl && service apache2 restart

fi

##############################################
# Begin Install ##############################
##############################################

# Create directories if dont exist ###########
mkdir -p /etc/ai
mkdir -p /usr/local/bin/ai_scripts


##############################################
# Install the .conf file as it # controls
# many of the file installation locations
# Backup if exists #todo Do i need to do this? I should make anther file if customisation is required..
#mv /usr/local/bin/ai_scripts/ai.conf /usr/local/bin/ai_scripts/ai.conf."$NOW".back
# Get the latest ################################
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/ai.conf -O /usr/local/bin/ai_scripts/ai.conf

# ref the conf file
source /usr/local/bin/ai_scripts/ai.conf # In case i need any vars
#################################################

# Add the vhost apache backup folder if no exist as the
# main ai script will use this
mkdir -p $BACKUP_FOLDER
#mkdir -p $BACKUP_FOLDER
mkdir -p $AI_CONFIG_BACKUP_FOLDER
mkdir -p $VHOST_BACKUP_FOLDER
mkdir -p $PASSWORDS_FOLDER
mkdir -p $WP_PASSWORDS_FOLDER
mkdir -p $TMP_FOLDER

# Add a couple of sym links to home folder ######
# mostly for debugging during dev
#todo do i need these?
#Add symbolic links in the home directory
#cd ~ && ln -s /usr/local/bin/ ai_script
#cd ~ && ln -s /usr/local/bin/ ai

# Fetch the latest version of the ai script #############
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/ai-script.sh -O /usr/local/bin/ai
chmod +x /usr/local/bin/ai

# Delete and replace ai apache directory for fresh install
rm -Rf /usr/local/bin/ai_scripts/apache && mkdir -p /usr/local/bin/ai_scripts/apache
rm -Rf /usr/local/bin/ai_scripts/nginx && mkdir -p /usr/local/bin/ai_scripts/nginx

# Download and replace all the latest files
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-add.sh -O /usr/local/bin/ai_scripts/apache/add
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-delete.sh -O /usr/local/bin/ai_scripts/apache/--DELETE
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-permissions.sh -O /usr/local/bin/ai_scripts/apache/site-permissions
#wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-disable.sh -O /usr/local/bin/ai_scripts/apache/site-disable
#wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-enable.sh -O /usr/local/bin/ai_scripts/apache/site-enable
#wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/site-reload.sh -O /usr/local/bin/ai_scripts/apache/site-reload
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/help.md -O /usr/local/bin/ai_scripts/help.md

# NGINX
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/nginx/site-add.sh -O /usr/local/bin/ai_scripts/nginx/site-add.sh
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/nginx/site-delete.sh -O /usr/local/bin/ai_scripts/nginx/site-delete.sh
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/nginx/site-ssl.sh -O /usr/local/bin/ai_scripts/nginx/site-ssl.sh
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/nginx/tools.sh -O /usr/local/bin/ai_scripts/nginx/tools.sh

#### Download latest ai apache config files
# vhost site skeletons
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/vhost/site_skeleton.sh -O /usr/local/bin/ai_scripts/apache/skeleton
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/vhost/site_skeleton-ssl.sh -O /usr/local/bin/ai_scripts/apache/skeleton-ssl
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/vhost/default.sh -O /usr/local/bin/ai_scripts/apache/site-default

# Installers
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/apache/installers/wordpress.sh -O /usr/local/bin/ai_scripts/apache/installer_wordpress

# Run backup installer as its basically part of the main module... #todo is this integrated properly. Needs to be
# part  of update and fresh install too.... Maybe that works just by having it here? passing the update var if it
# exists...
wget -qO ~/aibi gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/install.sh && bash ~/aibi $1

# Creating default 000-default config
bash /usr/local/bin/ai_scripts/apache/site-default

#######################
# Finish up
clear &&

ok "\n  Installation has completed...\n" &&

printf "\n  Ok thats done.... What now? Type ai for suggestions... \n\n"

# Delete installer
rm -f ~/aii

