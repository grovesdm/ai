#!/bin/bash
##############################################################
# DEVROCKET SCRIPT TO CREATE NGINX VHOST #####################
##############################################################


##############################################################
# SANITY CHECK ###############################################
##############################################################
[ $(id -g) != "0" ] && die "Script must be run as root."
[ $# != "1" ] && die "Usage: $(basename $0) domainName"


##############################################################
# Functions ##################################################
##############################################################
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }


##############################################################
# VARIABLES ##################################################
##############################################################
NGINX_AVAILABLE_VHOSTS='/etc/nginx/sites-available'
NGINX_ENABLED_VHOSTS='/etc/nginx/sites-enabled'
WEB_DIR='/var/www'
WEB_USER='www-data'
USER='daniel'
NGINX_SCHEME='$scheme'
NGINX_REQUEST_URI='$request_uri'


##############################################################
# CREATE NGINX VHOST FILE ####################################
##############################################################
echo "

server {
##################################################
# HTTP CONFIG ####################################
##################################################
  listen 80;
  listen [::]:80;
  server_name $1 www.$1;
  #return 301 https://\$server_name\$request_uri;
  root /var/www/$1/htdocs;
  index index.php index.htm index.html;

  location / {
    try_files \$uri \$uri/ /index.php;
  }

  location ~ \.php$ {
    proxy_pass http://localhost:9000;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    include snippets/fastcgi-php.conf;
    fastcgi_pass localhost:9000;
  }

  ################################
  # REFUSE ACCESS TO HTACCESS ####
  ################################
  location ~ /\.ht {
    deny all;
  }
}



server {
##################################################
# HTTPS CONFIG ###################################
##################################################
  listen 443 ssl http2;
  listen [::]:443 ssl http2;

  #ssl_certificate /etc/letsencrypt/live/$1/fullchain.pem;
  #ssl_certificate_key /etc/letsencrypt/live/$1/privkey.pem;

  ssl_session_cache shared:SSL:20m;
  ssl_session_timeout 60m;
  
  ### NEEDS MORE RESEARCH AND CONFIG
  #ssl_prefer_server_ciphers on;
  #ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DHE+AES128:!ADH:!AECDH:!MD5;
  #ssl_dhparam /etc/nginx/cert/dhparam.pem;
  #ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

  ssl_stapling on;
  ssl_stapling_verify on;
  #ssl_trusted_certificate /etc/nginx/cert/trustchain.crt;
  resolver 8.8.8.8 8.8.4.4;

  # Uncomment this line only after testing in browsers,
  # as it commits you to continuing to serve your site over HTTPS
  # in future
  # add_header Strict-Transport-Security "max-age=31536000";

  #access_log /var/log/nginx/access.log;
  #error_log /var/log/nginx/error.log;

  ##################################################
  # STANDARD CONFIG ################################
  ##################################################
  server_name $1 www.$1;
  root /var/www/$1/htdocs;
  index index.php index.htm index.html;

  location / {
    try_files \$uri \$uri/ /index.php;
  }

  location ~ \.php$ {
    proxy_pass http://localhost:9000;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    include snippets/fastcgi-php.conf;
    fastcgi_pass localhost:9000;
  }

  ################################
  # REFUSE ACCESS TO HIDDEN FILES
  ################################
  location ~ /\. {
    deny all;
  }
}

" > /var/www/$1/config/$1.nginx.conf

ln -s /var/www/$1/config/$1.nginx.conf $NGINX_AVAILABLE_VHOSTS/$1.conf


##############################################################
# ENABLE SITE BY CREATING SYMBOLIC LINK TO SITES ENABLED #####
##############################################################
ln -s $NGINX_AVAILABLE_VHOSTS/$1.conf $NGINX_ENABLED_VHOSTS/$1.conf


##############################################################
# RESTART NGINX AND SHOW STATUS ##############################
##############################################################
/etc/init.d/nginx restart;
ok "Site Created for $1"
/etc/init.d/nginx status