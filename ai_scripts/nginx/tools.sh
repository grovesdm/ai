#!/bin/bash

##############################################################
# VARIABLES ##################################################
##############################################################
NGINX_AVAILABLE_VHOSTS='/etc/nginx/sites-available'
NGINX_ENABLED_VHOSTS='/etc/nginx/sites-enabled'

##############################################################
# LIST THE NGINX VHOST FILES #################################
##############################################################
echo "AVAIL"
ls $NGINX_AVAILABLE_VHOSTS/ -l
echo "ENABLED"
ls $NGINX_ENABLED_VHOSTS/ -l