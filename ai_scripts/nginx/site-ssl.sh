#!/usr/bin/env bash

##############################################################
# CREATE A CERTBOT CERTIFICATE ###############################
##############################################################
certbot certonly --standalone -d $1 --agree-tos \
-m webmaster@$1 --pre-hook "systemctl stop nginx" \
--post-hook "systemctl start nginx"