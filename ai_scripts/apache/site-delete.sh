#!/bin/bash

# #################################################################
# SITE --DELETE ###################################################
###################################################################

# LOAD GLOBAL CONFIG ##############################################
source /usr/local/bin/ai_scripts/ai.conf
###################################################################

if [ "$1" = '' ]; then
	die "\n  Sorry I dont understand... Usage: ai $(basename $0) www.domain.name [ Options ] \n"

else

confirm "You sure you want to completely delete $TLD and all its related files?"

  sudo a2dissite $TLD
  if [ -f "$VHOSTDIR/$TLD$EXTENSION" ]; then
    sudo rm "$VHOSTDIR/$TLD$EXTENSION"
    ok "\n I deleted $VHOSTDIR/$TLD$EXTENSION \n"
  fi

  if [ -d "$WEBROOT/$TLD" ]; then
    sudo rm -rf "$WEBROOT/$TLD"
    ok "\n I deleted $WEBROOT/$TLD \n"
  else
    warn "$WEBROOT/$TLD does not exist"
  fi
#
#  if [ -f "${APACHE_LOG_DIR}/$TLD_access.log" ]; then
#    sudo rm "${APACHE_LOG_DIR}/$TLD_access.log"
#    echo "\n I deleted ${APACHE_LOG_DIR}/$TLD_access.log \n"
#  fi
#
#  if [ -f "${APACHE_LOG_DIR}/$TLD_error.log" ]; then
#    sudo rm "${APACHE_LOG_DIR}/$TLD_error.log"
#    echo "deleted ${APACHE_LOG_DIR}/$TLD_error.log"
#  fi

  $RESTARTCMD
  echo "
  I reloaded apache for you. You can thank me later ;-)
  "
fi

# DELETE THE NGINX CONF
bash /usr/local/bin/ai_scripts/nginx/site-delete.sh $TLD