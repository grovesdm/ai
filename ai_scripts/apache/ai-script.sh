#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

###########################################################################################
# Getting Started #########################################################################
###########################################################################################
# ai --help
if [ "$1" = '--help' ] || [ "$2" = '--help' ]; then
# Displays the contents of the help file
ok "
Help =======================" &&

cat /usr/local/bin/ai_scripts/help.md

die "
What would you like me to do..?
"

##########################################################################################
#Installers ##############################################################################
##########################################################################################
# ai --update || ai --install (does apt-get update)
elif [ "$1" = 'rocket' ] && [ "$2" = '--update' ]; then
wget -qO ~/aii gitlab.com/grovesdm/ai/raw/master/ai.sh && bash ~/aii rocket --update

elif [ "$1" = 'rocket' ] && [ "$2" = '--install' ]; then
wget -qO ~/aii gitlab.com/grovesdm/ai/raw/master/ai.sh && bash ~/aii rocket --install

# ai backup --install or ai backup --update
elif [ "$1" = 'backup' ]; then
	 if [ "$2" = '--install' ]; then
	wget -qO ~/aibi gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/install.sh && bash ~/aibi --install

	elif [ "$2" = '--update' ]; then
	wget -qO ~/aibi gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/install.sh && bash ~/aibi --update
	fi

###########################################################################################
# Apache ##################################################################################
###########################################################################################
# ai apache [-k start|restart|graceful|graceful-stop|stop]
elif [ "$1" = 'apache' ]; then

	# restart
	 if [ "$2" = 'restart'  ]; then
	sudo service apache2 restart

	echo -e "\033[32m
	  I have reloaded the Apache config for you..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# stop
	elif [ "$2" = 'stop'  ]; then
	sudo service apache2 stop

	echo -e "\033[32m
	  I have stopped the Apache Web Server for you..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# status
	elif [ "$2" = 'status' ]; then
	sudo service apache2 status

	echo -e "\033[32m
	  Thats the status of the Apache Web Server..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"

	# Start
	elif [ "$2" = 'start' ]; then
	sudo service apache2 start

	echo -e "\033[32m
	  Starting the Apache Web Server..." &&

	echo -e "\033[0m"

	echo "What would you like me to do..?
	"
	fi

###########################################################################################
# Site ###################################################################################
###########################################################################################
elif [ "$1" = 'site' ]; then

# Disable ##################################################
if [ "$2" = 'disable' ]; then
sudo a2dissite $3
sudo apachectl -k restart --graceful
echo "I have disabled site $3 for you."

# Enable ##################################################
elif [ "$2" = 'enable' ]; then
sudo a2ensite $3
sudo apachectl -k restart --graceful
echo "I have enabled site $3 for you."

# Site Reload ##################################################
elif [ "$2" = 'reload' ]; then
sudo a2dissite $3
sudo a2ensite $3
sudo apachectl -k restart --graceful
echo "I have reloaded site $3 for you."


# edit ##################################################
elif [ "$2" = 'edit' ]; then
TLD="$(echo "$3" | sed -r 's/www\.//')"
mcedit $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD$SITEVHOSTDIREXT
# this strips off the www

# list
	 elif  [ "$2" = 'list' ]; then

echo -e "\033[32m
Sites in the www folder
=======================" &&
echo -e "\033[0m"

ls /var/www -I html -I index.html | col -b
echo ""

echo -e "\033[32m
Enabled sites:
==============" &&
echo -e "\033[0m"

ls /etc/apache2/sites-enabled -I html -I index.html | col -b
echo ""


# QUICK AND DIRTY ADD NGINX
bash /usr/local/bin/ai_scripts/nginx/tools.sh

#################################################################
# Site Add ## With FTP
#################################################################
	elif [ "$2" = 'add' ]; then

	if [ "$4" = 'ftp' ] || [ "$5" = 'ftp' ]; then
	warn "I will create site $3 and ftp user $3" &&
	bash /usr/local/bin/ai_scripts/apache/add "$3" &&
	bash /usr/local/bin/ai_scripts/ftp/user-add "$3"

	ok "
	Done and done... Created site $3 and ftp user $3
	"


#################################################################
## Site Add ## Without FTP
#################################################################


# Passes FQDN to add
else
bash /usr/local/bin/ai_scripts/apache/add "$3" "$4" "$5"

#		# checks for www var to load correct skeleton file
#		if [ "$4" = 'www' ]; then
#		echo "
#		I will setup apache config for www.$3
#		"
#		bash /usr/local/bin/ai_scripts/apache/site-create-www "$3" "$5"
#		else
#		echo "
#		naked... Just the way I like it!
#		I will setup apache config as $3
#		"
#		bash /usr/local/bin/ai_scripts/apache/site-create "$3" "$4"
#		fi
		fi

	# Delete with ftp
	elif [ "$2" = '--DELETE' ] && [ "$4" = 'ftp' ]; then
	bash /usr/local/bin/ai_scripts/apache/--DELETE "$3"
	bash /usr/local/bin/ai_scripts/ftp/user-delete "$3"

	echo "
	deleted with ftp
	"

	# delete without ftp
	elif [ "$2" = '--DELETE' ]; then
	bash /usr/local/bin/ai_scripts/apache/--DELETE "$3"
echo "
	deleted without ftp
	"

	# All other backup and restore functions pass $var to aib
	elif [ "$2" = "backup" ]; then
	#todo add restore

		# if installed pass to aib

		if [ -f "/usr/local/bin/ai_scripts/backup/aib" ]; then
		bash /usr/local/bin/ai_scripts/backup/aib "$1" "$2" "$3"

		# else warning
		else
		echo -e "\033[32m
		It looks like the backup module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai backup --install'
		"
		fi

		elif [ "$2" = "restore" ]; then

		# if installed pass to aib

		if [ -f "/usr/local/bin/ai_scripts/backup/aib" ]; then
		bash /usr/local/bin/ai_scripts/backup/aib "$1" "$2" "$3"

		# else warning
		else
		echo -e "\033[32m
		It looks like the backup restore module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai backup --install'
		"
		fi
	fi

#########################################################################################
# FTP ###################################################################################
##### ######################################################################

elif [ "$1" = 'ftp' ]; then
 if [ "$2" = '--install' ]; then
# Run installer - Builds new '/usr/local/bin/ai_scripts/ftp' folder
wget -qO ~/aiftpi gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/install.sh && bash ~/aiftpi --install
 elif [ "$2" = '--update' ]; then
# Run installer - Builds new '/usr/local/bin/ai_scripts/ftp' folder
wget -qO ~/aiftpi gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/install.sh && bash ~/aiftpi --update

# Check installed
elif [ -d "/usr/local/bin/ai_scripts/ftp" ]; then


	if [ "$2" = 'add' ]; then

	bash /usr/local/bin/ai_scripts/ftp/user-add "$3"

	elif [ "$2" = 'password' ]; then

	bash /usr/local/bin/ai_scripts/ftp/user-add "$3"

	elif [ "$2" = '--DELETE' ]; then

	bash /usr/local/bin/ai_scripts/ftp/user-delete "$3"

	elif [ "$2" = 'list' ]; then

	bash /usr/local/bin/ai_scripts/ftp/user-list "$3"

fi
		# else warning
		else
		echo -e "\033[32m
		It looks like the ftp module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai ftp --install'
		"
		fi


##### Uninstall ###################################################
 elif [ "$1" = '--UNINSTALL' ] && [ "$2" = "--all" ]; then
 echo "
 Goodbye cruel world..........................
 So long.............

 Aaarrrgggg.................................... I'm melting...

 It hurts.......

 "
 rm -f /usr/local/bin/ai

 rm -Rf /usr/local/bin/ai_scripts/

 clear


echo -e "\033[32m
		The AI script has been deleted...."

		echo -e "\033[0m"

		echo "Backups have been left in the AI_FOLDER folder... Please delete as required.'
		"

exit 0

############################################################################################
# Firewall #################################################################################
############################################################################################
# Fresh Install
elif [ "$1" = 'firewall' ]; then
	if [ "$2" = '--install' ] || [ "$2" = '--update' ]; then
	wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/install.sh && bash ~/aifi "$2"

	else
	# All other firewall functions pass $var to aif
	# if installed pass to aif
		if [ -f "/usr/local/bin/ai_scripts/firewall/aif" ]; then
		bash /usr/local/bin/ai_scripts/firewall/aif "$1" "$2"

		# else warning
		else
		echo -e "\033[32m
		It looks like the firewall module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai firewall --install'
		"
		fi
	fi

############################################################################################
# Databse #################################################################################
############################################################################################
# Fresh Install
elif [ "$1" = 'db' ]; then
	if [ "$2" = '--install' ]; then
	wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --install
	elif [ "$2" = '--update' ]; then
	wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --update

	# run backup script if installed
	elif [ "$2" = 'backup' ]; then
	if [ -d "/usr/local/bin/ai_scripts/backup" ]; then
		bash /usr/local/bin/ai_scripts/backup/db_backup "$3"

		# else warning
		else
		echo -e "\033[32m
		It looks like the backup module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai backup --install'
		"
		fi


	else
	# All other db functions pass $var to aif
	# if installed pass to aidb
		if [ -f "/usr/local/bin/aidb" ]; then
		bash /usr/local/bin/aidb "$2" "$3" "$4"

		# else warning
		else
		echo -e "\033[32m
		It looks like the db module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai db --install'
		"
		fi
fi


###########################################################################################
# errors ##################################################################################
###########################################################################################
# no input
else


warn "
  Available Options:
  --help | --update | apache [command] | db [command] | firewall [command] | ai ftp [command] | ai site [command]" &&

ok "What would you like me to do..? "


fi
exit 0