#!/bin/bash

# LOAD GLOBAL CONFIG ##############################################
source /usr/local/bin/ai_scripts/ai.conf
###################################################################

# CHECK RUNNING AS ROOT ###########################################
[ $(id -g) != "0" ] && die "Script must be run as root."

# CHANGE OWNERSHIP
chown "$USRGRP" -Rf $WEBROOT/$1/htdocs/

# CHANGE PERMISSIONS TO 775 FOR FOLDERS AND 664 FOR FILES
# NOTE: GROUP NEEDS WRITE PERMISSION
chmod -Rf 775 $WEBROOT/$1/htdocs/
find $WEBROOT/$1/htdocs/ -type f -exec chmod 664 {} \;

# For TT you need to mod file permissions
chmod -f 777 $WEBROOT/$1/htdocs/WEB-INF/templates_c

# Statamic File permissions
sudo chmod 777 -Rf $WEBROOT/$1/htdocs/_cache $WEBROOT/$1/htdocs/_logs $WEBROOT/$1/htdocs/_content $WEBROOT/$1/htdocs/_config/users


ok "\n File permissions reset for: TT, Statamic and Wordpress... \n"

warn "\n Need to adjust any other file permissions? \n"