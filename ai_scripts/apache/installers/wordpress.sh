#!/bin/bash

# This script fetches WordPress plus some plugins with wegt, extracts everything
# and moves it in the right places. Afterwards the unnecessary ZIPs will be deleted.
#
# Option -e prints escape sequences like breaks \n

# run with bash /usr/local/bin/ai_scripts/apache/wordpress site.com
# needs vars for --wordpress in $5 and $6
#
source /usr/local/bin/ai_scripts/ai.conf

clear;
cd $TMP_FOLDER;
rm *
echo -e "\nMAKEDIR WordPress\n\n";

echo -e "\nFetching WordPress..\n\n";
wget http://wordpress.org/latest.zip -O latest.zip
unzip -o latest.zip;
cd wordpress;
rm readme.html license.txt;
cd ..;

#echo -e "\nFetching WP Touch...\n\n";
#wget --quiet --show-progress http://downloads.wordpress.org/plugin/wptouch.zip;
#unzip -q wptouch.zip;
#mv wptouch wordpress/wp-content/plugins/;
#
#echo -e "\nFetching WordPress SEO...\n\n";
#wget --quiet --show-progress http://downloads.wordpress.org/plugin/wordpress-seo.zip;
#unzip -q wordpress-seo.zip;
#mv wordpress-seo wordpress/wp-content/plugins/;
#
#echo -e "\nFetching Akismet...\n\n";
#wget --quiet --show-progress http://downloads.wordpress.org/plugin/akismet.zip;
#unzip -q akismet.zip;
#mv wordpress-seo wordpress/wp-content/plugins/;
#
echo -e "\n IWP Client...\n\n";
wget https://downloads.wordpress.org/plugin/iwp-client.zip;
unzip -o iwp-client.zip -d wordpress/wp-content/plugins/;
#cp -r iwp-client wordpress/wp-content/plugins/;

#rm wordpress-seo.zip;
#rm contact-form-7.zip;
#rm wptouch.zip;
#rm latest-de_DE.zip;
#rm akismet.zip;
rm -f iwp-client.zip

rm -rf /var/www/"$TLD"/htdocs/*
cp -r wordpress/. /var/www/"$TLD"/htdocs
rm -fr wordpress
rm -f latest.zip


