#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf
TLD="$(echo "$1" | sed -r 's/www\.//')"

echo "
############################################################################
# You can generate a new key by:
# openssl req -nodes -newkey rsa:2048 -keyout $TLD.key -out $TLD.csr
# outputs a key and a copy of the certificate signing request
# change to to site root
# cd /var/www/$TLD
# unzip the ssl archive
# unzip ssl.zip
# write the contents of each of the files to a new file '$TLD.ca-bundle'
# cat COMODORSADomainValidationSecureServerCA.crt COMODORSAAddTrustCA.crt AddTrustExternalCARoot.crt >> $TLD.ca-bundle
############################################################################

# skip this if module is not working to prevent apache crash
<IfModule mod_ssl.c>
<VirtualHost *:443>

    ServerAdmin webmaster@$TLD
    ServerName $TLD
    DocumentRoot /var/www/$TLD/htdocs

############################################################################
#### These should be set in the default Apache config but better to be safe
		<DIRECTORY "/">
			# Ensure that files outside the web root are not served
			Require all denied
		</DIRECTORY>

		<Files ".ht*">
			# Deny read access to .htaccess files
			Require all denied
		</Files>
############################################################################

	<DIRECTORY /var/www/$TLD/htdocs>

		# Allow public access to site
		Require all granted

		# Turn off directory browsing
		Options -Indexes

		# Read .htaccess AllowOverride None = no and AllowOverride All = yes
		# AllowOverride None
		AllowOverride All

	</DIRECTORY>

    # SSL Engine Switch:
    # Enable/Disable SSL for this virtual host.
    SSLEngine on

    ####################################################################
    # Certificate setup
    # =================
    # all 3 files are required:
    # 1. The Key. Name must be  = $TLD.key
    # 2. The 'Domain Certificate' = $TLD.crt
    # 3. The CA (Certificate Authority) Bundle = $TLD.ca-bundle
    # Dont edit this file rather change the name of the files as below.
    SSLCertificateKeyFile /var/www/$TLD/cert/$TLD.key
    SSLCertificateFile /var/www/$TLD/cert/$TLD.crt
    SSLCACertificateFile /var/www/$TLD/cert/$TLD.ca-bundle
	#####################################################################


	ErrorLog $WEBROOT/$TLD/$SITELOGS/$TLD-error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

    # Custom Log location + options
	CustomLog $WEBROOT/$TLD/$SITELOGS/$TLD-access.log combined env=!dontlog

</VirtualHost>
</IfModule>
" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD-ssl$SITEVHOSTDIREXT


