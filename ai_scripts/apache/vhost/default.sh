#!/bin/bash
##################################################
# Redirect to with or without www ################
##################################################

# Load Config
source /usr/local/bin/ai_scripts/ai.conf


# Overwrite existing 000-default config
echo "
###################################################################
# DO NOT EDIT THIS FILE AS ALL CHANGES WILL BE LOST ###############
# LOAD SITE VHOST FROM SITE FOLDER ################################
###################################################################
Include $WEBROOT/000-default/$SITEVHOSTDIR/000-default$SITEVHOSTDIREXT
" > $VHOSTDIR/000-default$EXTENSION

# Overwrite existing default-ssl config
echo "
###################################################################
# DO NOT EDIT THIS FILE AS ALL CHANGES WILL BE LOST ###############
# LOAD SITE VHOST FROM SITE FOLDER ################################
###################################################################
Include $WEBROOT/000-default/$SITEVHOSTDIR/000-default$SITEVHOSTDIREXT
" > $VHOSTDIR/default-ssl$EXTENSION

# Delete everything to start again
rm -rf $WEBROOT/html
rm -rf $WEBROOT/000-default


ok "\n Rebuilt Apache 'sites-available' vhost file $WEBROOT/000-default/$SITEVHOSTDIR/000-default$SITEVHOSTDIREXT \n"

# MAKE WEB ROOT DIRECTORY STRUCTURE

mkdir "$WEBROOT/000-default"
mkdir "$WEBROOT/000-default/htdocs"
mkdir "$WEBROOT/000-default/config"
mkdir "$WEBROOT/000-default/logs"

# Creating index.html file
cat > $WEBROOT/000-default/htdocs/index.html <<EOF
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="assets_landing-page/style.css">
	<title>Apache Rocket - Server</title>

	<style>
		* {
			margin: 0;
		}

		html {
			max-width: 100%;
		}

		body {
			background-image: url("http://cdn.slyfoxmedia.com.au.s3.amazonaws.com/apache_rocket/assets_landing-page/server-rocket.png"), url("http://cdn.slyfoxmedia.com.au.s3.amazonaws.com/apache_rocket/assets_landing-page/server-bg.jpg");
			background-position: center center;
			background-repeat: no-repeat;
			background-size: cover;
			height: 100vh;

		}
	</style>
</head>
<body>

</body>
</html>
EOF

# ADD ROBOTS
echo "
User-agent: *
Disallow: /

" > $WEBROOT/000-default/htdocs/robots.txt

echo "

# This default file is required to prevent the first vhost being displayed if the
# requested site doesn't exist. The same for SSL otherwise it tries to use the firstavailable SSL certificate which will fail and looks stupid!

<VirtualHost *:80>

    ServerAdmin webmaster@localhost
	DocumentRoot /var/www/000-default/htdocs
	
############################################################################
#### These should be set in the default Apache config but better to be safe
		<DIRECTORY "/">
			# Ensure that files outside the web root are not served
			Require all denied
		</DIRECTORY>

		<Files ".ht*">
			# Deny read access to .htaccess files
			Require all denied
		</Files>
############################################################################

	<DIRECTORY /var/www/000-default/htdocs>

		# Allow public access to site
		Require all granted

		# Turn off directory browsing
		Options -Indexes

		# Read .htaccess AllowOverride None = no and AllowOverride All = yes
		AllowOverride None
		# AllowOverride All

	</DIRECTORY>

	ErrorLog $WEBROOT/000-default/$SITELOGS/000-default-error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

	# Custom Log location + options
	CustomLog $WEBROOT/000-default/$SITELOGS/000-default-access.log combined env=!dontlog

</VirtualHost>

" >> $WEBROOT/000-default/$SITEVHOSTDIR/000-default$SITEVHOSTDIREXT


echo "

# This default file is required to prevent the first vhost being displayed if the
# requested site doesn't exist. The same for SSL otherwise it tries to use the first
# available SSL certificate which will fail and looks stupid!

<IfModule mod_ssl.c>
<VirtualHost _default_:443>

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/000-default/htdocs

############################################################################
#### These should be set in the default Apache config but better to be safe
		<DIRECTORY "/">
			# Ensure that files outside the web root are not served
			Require all denied
		</DIRECTORY>

		<Files ".ht*">
			# Deny read access to .htaccess files
			Require all denied
		</Files>
############################################################################

	<DIRECTORY /var/www/000-default/htdocs>

		# Allow public access to site
		Require all granted

		# Turn off directory browsing
		Options -Indexes

		# Read .htaccess AllowOverride None = no and AllowOverride All = yes
		# AllowOverride None
		AllowOverride All

	</DIRECTORY>

    # SSL Engine Switch:
    # Enable/Disable SSL for this virtual host.
    SSLEngine on

    ####################################################################
    # Certificate setup
    # =================
    # all 3 files are required:
    # 1. The Key. Name must be  = 000-default.key
    # 2. The 'Domain Certificate' = 000-default.crt
    # 3. The CA (Certificate Authority) Bundle = 000-default.ca-bundle
    # Dont edit this file rather change the name of the files as below.
    #SSLCertificateKeyFile /var/www/000-default/cert/000-default.key
    #SSLCertificateFile /var/www/000-default/cert/000-default.crt
    #SSLCACertificateFile /var/www/000-default/cert/000-default.ca-bundle
	#####################################################################


</VirtualHost>
</IfModule>
" >> $WEBROOT/000-default/$SITEVHOSTDIR/default-ssl$SITEVHOSTDIREXT

# ENABLE SITE AND RELOAD APACHE
sudo a2ensite 000-default && sudo a2ensite default-ssl && $RESTARTCMD && ok "[ Restarted Apache! ]"

