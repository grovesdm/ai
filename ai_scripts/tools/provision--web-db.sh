#! /bin/bash
############################################################
# CHECK FOR PACKAGES AND UPDATE ############################
############################################################
apt update
apt upgrade -y
apt dist-upgrade -y


############################################################
# INSTALL APACHE AND CHANGE PORTS TO PREVENT SCRIPT FAIL ###
############################################################
apt install apache2 -y
##################################################
# CREATE NEW APACHE PORTS FILE ###################
##################################################
cat <<EOF > /etc/apache2/ports.conf
##################################################
# CUSTOM PORTS FOR NGINX REVERSE PROXY ###########
##################################################
Listen 8080

<IfModule ssl_module>
    Listen 8443
</IfModule>

<IfModule mod_gnutls.c>
    Listen 8443
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
##################################################
# ADDED BY DEVROCKET... TRAINING-WHEELS FOR DEVOPS
##################################################
EOF
##################################################
# RESTART APACHE ON NEW PORTS ####################
##################################################
systemctl restart apache2 &&
sleep 5

############################################################
# INSTALL REQUIRED PACKAGES ################################
############################################################
declare -a PACKAGES=(
nginx
php7.0
php7.0-opcache
php-apcu
php-pear
php7.0-curl
php7.0-mcrypt
php7.0-gd
php-fpm
php7.0-mysql
libapache2-mod-php7.0
mysql-client
ssmtp
zip
unzip
dphys-swapfile
haveged
certbot
mysql-server
mc
tmux
)
##################################################
# LOOP THROUGH ABOVE PACKAGES AND INSTALL ########
##################################################
for PKG in "${PACKAGES[@]}"
do
   apt install -y "$PKG"
done



############################################################
# APPEND CONFIG TO APACHE IF ITS NOT THERE #################
############################################################
grep -q -F 'SetHandler "proxy:fcgi://localhost:9000/' /etc/apache2/apache2.conf || \
sed -i '/<\/FilesMatch>/a \
\
\
##################################################\
# ADDED BY DEVROCKET... TRAINING-WHEELS FOR DEVOPS\
##################################################\
<FilesMatch "\.php$">\
    SetHandler "proxy:fcgi://localhost:9000/"\
</FilesMatch>\
##################################################\
# ADDED BY DEVROCKET... TRAINING-WHEELS FOR DEVOPS\
##################################################\
\
' /etc/apache2/apache2.conf


############################################################
# UPDATE PHP-FPM CONFIG - SAFE TO RUN MORE THAN ONCE #######
############################################################
# LOOKS FOR LINE IN FILE AND COMMENTS IF THERE ####
###################################################
sed -e '/listen = \/run\/php\/php7.0-fpm.sock/ s/^;*/;/' -i /etc/php/7.0/fpm/pool.d/www.conf
###################################################
# LOOKS FOR LINE IN FILE AND ADDS IF NOT THERE ####
###################################################
grep -q -F 'listen = localhost:9000' /etc/php/7.0/fpm/pool.d/www.conf || \
sed -i '/;listen = \/run\/php\/php7.0-fpm.sock/a listen = localhost:9000' /etc/php/7.0/fpm/pool.d/www.conf


############################################################
# UPDATE THE DEFAULT NGINX VHOST ROOT ######################
###################################################
# COMMENTS THE ORIGINAL ROOT ######################
###################################################
sed -e '/root \/var\/www\/html;/ s/^#*/#/' -i /etc/nginx/sites-available/default
###################################################
# LOOKS FOR LINE IN FILE AND ADDS IF NOT THERE ####
###################################################
grep -q -F 'root /var/www/000-default/htdocs;' /etc/nginx/sites-available/default || \
sed -i '/root \/var\/www\/html;/a root \/var\/www\/000-default\/htdocs;' /etc/nginx/sites-available/default


############################################################
# ENABLE FCGI APACHE MODULE ################################
############################################################
a2enmod proxy_fcgi


############################################################
# RESTART SERVICES #########################################
############################################################
systemctl restart php7.0-fpm apache2 nginx mysql


############################################################
# INSTALL DEVROCKET ########################################
############################################################
wget -qO ~/aii gitlab.com/grovesdm/ai/raw/master/ai.sh && bash ~/aii

# LINK
#wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/tools/provision--web-db.sh -O ~/
