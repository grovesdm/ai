#!/bin/bash

# Install ##################################################
if [ "$2" = '--install' ] || [ "$2" = '--update' ]; then
wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/install.sh && bash ~/aifi "$2"

exit 0

elif [ "$2" = '' ]; then
    echo "What would you like to do with the $1..?


    "

# Update ######################################################
elif [ "$2" == '--update' ]; then

wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/aif.sh && bash ~/aifi --update

exit 0

##### Uninstall ######################################################
elif [ "$2" == '--uninstall' ]; then

rm -rf /usr/local/bin/ai_scripts/firewall

# Modifies the startup file and creates a backup (use -i.bak to create a backup)
sed -i '/ip_conntrack_ftp/d' /etc/modules

echo -e "\033[32m
Sucessfully uninstalled. The config backups have not been deleted.
They are stored in the 'ai' folder in your home directory
" && echo -e "\033[0m"


	##### Load Web Server Config ######################################################
	elif [ "$2" = '--web' ]; then

	bash /usr/local/bin/ai_scripts/firewall/web_server "$1"

	##### Load DB Server Config ######################################################
	elif [ "$2" = '--db' ]; then

	bash /usr/local/bin/ai_scripts/firewall/db_server "$1"

	##### Reset IP Tables ######################################################
	elif [ "$2" = '--reset' ]; then

	bash /usr/local/bin/ai_scripts/firewall/reset "$1"

		##### Reset IP Tables ######################################################
	elif [ "$2" = '--status' ]; then

	echo -e "\033[32m
Current 'iptables' firewall rules..." && echo -e "\033[0m"

iptables -L

echo -e "\033[32m
And the Open Ports
==================" && echo -e "\033[0m"


sudo nmap localhost



fi


