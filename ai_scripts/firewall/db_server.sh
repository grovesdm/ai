#!/bin/bash

echo -e "\033[32m
Updating the firewall iptables for a database server...\n" &&

echo "Wont be too long...
"

##############################################################
# General
##############################################################

# Delete all existing rules
iptables -F

###########################################################################
# Drop all other traffic - default deny unless explicitly allowed policy
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP
###########################################################################


# Allow Loopback Connections
#The loopback interface, also referred to as lo, is what a computer uses to for network connections to itself. For example, if you run ping localhost or ping 127.0.0.1, your server will ping itself using the loopback. The loopback interface is also used if you configure your application server to connect to a database server with a "localhost" address. As such, you will want to be sure that your firewall is allowing these connections.
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT


#Allow Established and Related Incoming Connections
#As network traffic generally needs to be two-way—incoming and outgoing—to work properly, it is typical to create a firewall rule that allows established and related incoming traffic, so that the server will allow return traffic to outgoing connections initiated by the server itself. This command will allow that:
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Allow Established Outgoing Connections
#You may want to allow outgoing traffic of all established connections, which are typically the response to legitimate incoming connections. This command will allow that:
iptables -A OUTPUT -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT


#Internal to External
#Assuming eth0 is your external network, and eth1 is your internal network, this will allow your internal to access the external:
iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT


#Drop Invalid Packets
#Some network traffic packets get marked as invalid. Sometimes it can be useful to log this type of packet but often it is fine to drop them. Do so with this command:
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

# Restrict access to port 111
iptables -A INPUT -p tcp --dport 111 -j REJECT
iptables -A INPUT -p udp --dport 111 -j REJECT

# Ping from inside to outside
iptables -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT

# Ping from outside to inside
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT


# Prevent DoS attack
#iptables -A INPUT -p tcp --dport 80 -m limit --limit 25/minute --limit-burst 100 -j ACCEPT


# Block an IP Address
#To block network connections that originate from a specific IP address, 15.15.15.51 for example, run this command:
#iptables -A INPUT -s 15.15.15.51 -j DROP

#In this example, -s 15.15.15.51 specifies a source IP address of "15.15.15.51". The source IP address can be specified in any firewall rule, including an allow rule.
#
#If you want to reject the connection instead, which will respond to the connection request with a "connection refused" error, replace "DROP" with "REJECT" like this:
#iptables -A INPUT -s 15.15.15.51 -j REJECT

#Block Connections to a Network Interface
#To block connections from a specific IP address, e.g. 15.15.15.51, to a specific network interface, e.g. eth0, use this command:
#iptables -A INPUT -i eth0 -s 15.15.15.51 -j DROP

#This is the same as the previous example, with the addition of -i eth0. The network interface can be specified in any firewall rule, and is a great way to limit the rule to a particular network.


##############################################################
# Service: SSH
##############################################################
#If you're using a cloud server, you will probably want to allow incoming SSH connections (port 22) so you can connect to and manage your server. This section covers how to configure your firewall with various SSH-related rules.

# Allow All Incoming SSH
#To allow all incoming SSH connections run these commands:
iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT
#The second command, which allows the outgoing traffic of established SSH connections, is only necessary if the OUTPUT policy is not set to ACCEPT.


#Allow Incoming SSH from Specific IP address or subnet
#To allow incoming SSH connections from a specific IP address or subnet, specify the source. For example, if you want to allow the entire 15.15.15.0/24 subnet, run these commands:
#iptables -A INPUT -p tcp -s 15.15.15.0/24 --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT
#The second command, which allows the outgoing traffic of established SSH connections, is only necessary if the OUTPUT policy is not set to ACCEPT.


# Allow Outgoing SSH
# If your firewall OUTPUT policy is not set to ACCEPT, and you want to allow outgoing SSH connections—your server initiating an SSH connection to another server—you can run these commands:
iptables -A OUTPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT


#Allow Incoming Rsync from Specific IP Address or Subnet
# Rsync, which runs on port 873, can be used to transfer files from one computer to another.
#To allow incoming rsync connections from a specific IP address or subnet, specify the source IP address and the destination port. For example, if you want to allow the entire 15.15.15.0/24 subnet to be able to rsync to your server, run these commands:
#iptables -A INPUT -p tcp -s 15.15.15.0/24 --dport 873 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 873 -m conntrack --ctstate ESTABLISHED -j ACCEPT
#The second command, which allows the outgoing traffic of established rsync connections, is only necessary if the OUTPUT policy is not set to ACCEPT.


# Allow FTP
#iptables -A INPUT -p tcp -m tcp --dport 21 -j ACCEPT
#iptables -A INPUT -p tcp -m tcp --dport 20 -j ACCEPT



##############################################################
# Service: MySQL
##############################################################

#MySQL listens for client connections on port 3306. If your MySQL database server is being used by a client on a remote server, you need to be sure to allow that traffic.

#Allow MySQL from Specific IP Address or Subnet
#To allow incoming MySQL connections from a specific IP address or subnet, specify the source. For example, if you want to allow the entire 15.15.15.0/24 subnet, run these commands:
#iptables -A INPUT -p tcp -s 15.15.15.0/24 --dport 3306 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 3306 -m conntrack --ctstate ESTABLISHED -j ACCEPT

#The second command, which allows the outgoing traffic of established MySQL connections, is only necessary if the OUTPUT policy is not set to ACCEPT.

#Allow MySQL to Specific Network Interface
#To allow MySQL connections to a specific network interface—say you have a private network interface eth1, for example—use these commands:
iptables -A INPUT -i eth1 -p tcp --dport 3306 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth1 -p tcp --sport 3306 -m conntrack --ctstate ESTABLISHED -j ACCEPT
#The second command, which allows the outgoing traffic of established MySQL connections, is only necessary if the OUTPUT policy is not set to ACCEPT.


##############################################################
# Service: Mail
##############################################################
# Mail servers, such as Sendmail and Postfix, listen on a variety of ports depending on the protocols being used for mail delivery. If you are running a mail server, determine which protocols you are using and allow the appropriate types of traffic. We will also show you how to create a rule to block outgoing SMTP mail.

#Block Outgoing SMTP Mail
#If your server shouldn't be sending outgoing mail, you may want to block that kind of traffic. To block outgoing SMTP mail, which uses port 25, run this command:
#iptables -A OUTPUT -p tcp --dport 25 -j REJECT
#This configures iptables to reject all outgoing traffic on port 25. If you need to reject a different service by its port number, instead of port 25, simply replace it.

#Allow All Incoming SMTP

#To allow your server to respond to SMTP connections, port 25, run these commands:
#iptables -A INPUT -p tcp --dport 25 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 587 -m conntrack --ctstate ESTABLISHED -j ACCEPT
#The second command, which allows the outgoing traffic of established SMTP connections, is only necessary if the OUTPUT policy is not set to ACCEPT.
#Note: It is common for SMTP servers to use port 587 for outbound mail.



# Save new rules
sudo invoke-rc.d netfilter-persistent save

# Start the IP Tables persistent service
service netfilter-persistent start

clear

echo -e "\033[32m
I setted the rules for you see..." && echo -e "\033[0m"

iptables -L

echo -e "\033[32m
And the Open Ports
==================" && echo -e "\033[0m"


sudo nmap localhost

echo "You are setup as a DB server now!
"