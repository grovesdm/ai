Getting Started
===============
    ai --help                           # display help file
    ai rocket --install                        # or 're install' incl dependencies
    ai rocket --update                         # update the script

Apache
======
    ai apache start                     # shows apache status
    ai apache stop                      # shows apache status
    ai apache restart                   # restarts the apache server  
    ai apache status                    # shows apache status  

Site
====
    ai site list                        # displays a list of all sites in the var/www folder
    ai site add example.com             # adds the site example.com to var/www and creates apache vhost config
                                        # Note: creates a sample index.html and test.php page
    ai site add example.com ftp         # adds site and creates ftp user with username example.com
    ai site --DELETE example.com        # deletes the site folder and apache config
    ai site --DELETE example.com ftp    # same but also deletes the ftp user
    ai site disable example.com         # disables site
    ai site enable example.com          # enables site
    ai site reload example.com          # reloads site config
    ai site edit gizmo.com              # opens /etc/apache2/sites-available/www.gizmo.com in mcedit
    ai site backup example.com          # backs up var/www/example.com
    ai site restore example.com         # restores the backup to var/www/example.com
    
`Note: Adding a site that already exists is non destructive so it can be used to rebuild the apache config file.`
 
Database
--------
    ai db add db_name                   # (also use to create a new user wont affect existing db)
    ai db --DELETE db_name db_user      # you need to enter username just to be safe
    ai db list                          # List available databases
    ai db user list                     # List MysQL users
    ai db user --PASSWORD db_user       # change password
    ai db user --DELETE db_user         # delete user
    ai db --install                     # fresh install db module
    ai db --update                      # update db module
    ai db backup example.com            # Dumps a copy of the db ai_com to backup folder
    ai db --RESTORE example.com         # restores the backup to database (DB needs to exist first)
    ai db stop                          # stops the database server
    ai db start                         # starts the database server
    ai db restart                       # restarts the database status
    ai db status                        # shows the database status
Firewall
========
    ai firewall --install               # fresh install of script and update of dependencies
    *ai firewall --UNINSTALL            # completely removes the ai firewall scripts
    ai firewall --update                # Updates firewall scripts
    ai firewall --reset                 # flushes all rules and saves config 
    ai firewall --web                   # saves the default config for web server
    ai firewall --db                    # saves the default config for db server
    ai firewall --status                # shows the current 'iptable' rules and open ports

vsFTP
=====
    ai ftp --install                    # install installs ftp and its dependencies
    ai ftp add example.com              # add a new ftp user example.com with access to /var/www/example.com
    ai ftp --DELETE example.com         # deletes ftp user example.com
    ai ftp password example.com         # (resets the ftp password for user gizmo.com)
    ai ftp list                         # opens list of all ftp users and passwords
    *ai ftp --UNINSTALL                 # completely removes the ai firewall scripts
    
Uninstall
=========
    *ai --UNINSTALL --all               # will remove everything but leave the sites folder
    
All items with a star are not yet implemented **
    