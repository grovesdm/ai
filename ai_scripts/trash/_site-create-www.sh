#!/bin/bash

# get the config
source /usr/local/bin/ai_scripts/ai.conf

# if $1 is not blank
if [ "$1" != '' ]; then

# if the sites-available file doesnt exist
if [ ! -f "$VHOSTDIR$1$EXTENSION" ]; then
# copy to sites-available
cp "/usr/local/bin/ai_scripts/apache/vhost" "$VHOSTDIR$1$EXTENSION"
echo "created $VHOSTDIR$1$EXTENSION"
else
# make a backup of old and replace
      mv "$VHOSTDIR$1$EXTENSION" "$VHOSTDIR$1$EXTENSION.bak"
cp "/usr/local/bin/ai_scripts/apache/vhost" "$VHOSTDIR$1$EXTENSION"
#      cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$VHOSTDIR$1$EXTENSION"
echo "created $VHOSTDIR$1$EXTENSION and made a backup of the existing conf"
fi

# then replace SKELETON with the domain name and the correct location of the conf file we just
# created
#find "$VHOSTDIR$1$EXTENSION" -type f -exec sed -i "s/SKELETON/$1/" {} \;
#find "$VHOSTDIR$1$EXTENSION" -type f -exec sed -i "s/apache.conf/apache-www\.conf/" {} \;

# if no web root then make one
if [ ! -d "$WEBROOT$1/" ]; then
mkdir "$WEBROOT$1/"
chmod -w "$WEBROOT$1/"
mkdir "$WEBROOT$1/config/"
mkdir "$WEBROOT$1/htdocs"

# copy the actual apache config to webroot
cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$WEBROOT$1/config/apache.conf"
# Find and rename the domain name
find "$WEBROOT$1/config/apache.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;
# same for ssl file
cp "/usr/local/bin/ai_scripts/apache/skeleton-www-ssl" "$WEBROOT$1/config/apache-ssl.conf"
# Find and rename domain name
find "$WEBROOT$1/config/apache-www-ssl.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;

# if #2 is for wordpress install then do that first
if [ "$2" = "--wordpress" ]; then
bash /usr/local/bin/ai_scripts/apache/wordpress "$1"
#		bash /usr/local/bin/ai_scripts/apache/site-create "$1"
# if not wordpress then install default pages to see site working
else
cp "/usr/local/bin/ai_scripts/apache/index.html" "$WEBROOT$1/htdocs"
cp "/usr/local/bin/ai_scripts/apache/test.php" "$WEBROOT$1/htdocs"
fi



#	chmod -R 775 "$WEBROOT$1/htdocs"
#todo need to check this. I changed to 775 and 664 as I made usergrp www-data:www-data to get wordpress to work and via ftp
chmod -Rf 775 $WEBROOT"$1"/htdocs/
find $WEBROOT$1/htdocs/ -type f -exec chmod 664 {} \;
chown -R $USRGRP "$WEBROOT$1/htdocs"
echo "created $WEBROOT$1/"

else
echo "$WEBROOT$1/ already exists...
I reinstalled the apache config to $WEBROOT$1/config/apache.conf and made a backup of what was there...
Please check the site is working before running this again as I only ever keep 1 backup"

# Check for apache config file in web directory if the directory exists
if [ ! -f "$WEBROOT$1/config/apache.conf" ]; then
mkdir -p "$WEBROOT$1/config/"
cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$WEBROOT$1/config/apache.conf"
# Find and rename the domain name
find "$WEBROOT$1/config/apache.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;
echo "created $WEBROOT$1/config/apache.conf
"
# same for ssl file
cp "/usr/local/bin/ai_scripts/apache/skeleton-www-ssl" "$WEBROOT$1/config/apache-ssl.conf"
# Find and rename domain name
find "$WEBROOT$1/config/apache-ssl.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;
#      cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$VHOSTDIR$1$EXTENSION"
echo "created $WEBROOT$1/config/apache.conf and made a backup of the existing file
"

else
# if it does make a backup and install again
mv "$WEBROOT$1/config/apache.conf" "$WEBROOT$1/config/apache.conf.bak"
cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$WEBROOT$1/config/apache.conf"
# Find and rename the domain name
find "$WEBROOT$1/config/apache.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;

# same for ssl file
cp "/usr/local/bin/ai_scripts/apache/skeleton-www-ssl" "$WEBROOT$1/config/apache-ssl.conf"
# Find and rename domain name
find "$WEBROOT$1/config/apache-ssl.conf" -type f -exec sed -i "s/SKELETON/$1/" {} \;
#      cp "/usr/local/bin/ai_scripts/apache/skeleton-www" "$VHOSTDIR$1$EXTENSION"
echo "created $WEBROOT$1/config/apache.conf and made a backup of the existing file
"
fi
fi
sudo a2ensite $1
$RESTARTCMD
echo "reloaded apache"
fi

