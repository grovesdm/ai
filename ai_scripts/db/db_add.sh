#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

#Can be run to create a new user with privs and is non destructive to existing database

EXPECTED_ARGS=1
E_BADARGS=65
MYSQL=`which mysql`
#now="$(date +"%Y-%m-%d %H:%M:%S")"

DB="$(echo "$1" | sed -r 's/[.]+/_/g')"
PASS="$(makepasswd --chars 20)"
RAND="$(makepasswd --chars 5)"
USER="${DB:0:10}"$RAND

Q1="CREATE DATABASE IF NOT EXISTS $DB;"
Q2="GRANT USAGE ON *.* TO '$USER'@'%' IDENTIFIED BY '$PASS';"
Q3="GRANT ALL PRIVILEGES ON $DB.* TO $USER@'%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

if [ $# -eq $EXPECTED_ARGS ]; then

echo -e "\033[32m
Created DB $DB with user $USER and Password $PASS
" && echo -e "\033[0m"

echo "
User created on $NOW
===============================
DB User: $USER
User Password: $PASS
Full Access to DB: $DB" >> $DB_PASSWORDS_FOLDER/$DB.txt

#todo chmod to 600 ?

echo "
Ok thats done....
"

elif [ $# -ne $EXPECTED_ARGS ]; then
  echo "Usage: $0 Enter the site name (without www) and I will do everything else..."
#  exit $E_BADARGS
exit 0
fi

#$MYSQL -uroot -p"xsv42k" -e "$SQL"
$MYSQL -e "$SQL"


