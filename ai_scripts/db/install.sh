#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

# Install dependencies for mysql/ db server
apt-get install makepasswd -y --force-yes

#todo add installer for mysql server as per https://www.evernote.com/shard/s257/nl/34040516/30e86bed-843f-406e-a440-f8d9c4c7450d

# Make directories
mkdir -p /usr/local/bin/ai_scripts
mkdir -p /usr/local/bin/ai_scripts/db
mkdir -p /etc/ai
mkdir -p /etc/ai/passwords
mkdir -p /etc/ai/passwords/db

# Download scripts

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/db_add.sh -O /usr/local/bin/ai_scripts/db/db_add

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/db_del.sh -O /usr/local/bin/ai_scripts/db/db_del

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/db_list.sh -O /usr/local/bin/ai_scripts/db/db_list

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/user_list.sh -O /usr/local/bin/ai_scripts/db/user_list

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/user_del.sh -O /usr/local/bin/ai_scripts/db/user_del

wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/user_pass.sh -O /usr/local/bin/ai_scripts/db/user_pass

wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/aidb.sh -O /usr/local/bin/aidb
chmod +x /usr/local/bin/aidb


# Add the .conf file only if it doesn't already exist.
wget -nc https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/ai.conf -O /usr/local/bin/ai_scripts/ai.conf

# Make the directories that rely on the conf file
mkdir -p $AI_FOLDER

mkdir -p $PASSWORDS_FOLDER
mkdir -p $DB_PASSWORDS_FOLDER

# Delete the installer
rm -f ~/aidbi

clear &&

echo -e "\033[32m
Installation of the ai db module has completed...
" &&
echo -e "\033[0m"
echo "
Ok thats done.... let's make some sh!t
"
