#!/bin/bash

now="$(date +"%Y-%m-%d %H:%M:%S")"
password="$(makepasswd --chars 20)"

mysql << EOF
SET PASSWORD FOR '$1'@'%' = PASSWORD('$password');
FLUSH PRIVILEGES;
exit
EOF

echo "Password for user $1 has been changed to $password"

echo "$now - Changed password for $1 to $password" >> /etc/ai/db/user_pass.txt

