#!/bin/bash


# Needed to update if installed as stand alone
	if [ "$1" = '--install' ]; then
	wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --install
	elif [ "$1" = '--update' ]; then
	wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --update

# Install Firewall or run if installed

	elif [ "$1" = 'firewall' ]; then
	if [ "$2" = '--install' ] || [ "$2" = '--update' ]; then
	wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/install.sh && bash ~/aifi "$2"

	else
	# All other firewall functions pass $var to aif
	# if installed pass to aif
		if [ -f "/usr/local/bin/ai_scripts/firewall/aif" ]; then
		bash /usr/local/bin/ai_scripts/firewall/aif "$1" "$2"

		# else warning
		else
		echo -e "\033[32m
		It looks like the firewall module is not installed..."

		echo -e "\033[0m"

		echo "  You can install it with 'ai firewall --install'
		"
		fi
	fi






# Create a new database or create a new user for an existing DB
elif [ "$1" = "add" ]; then
sudo bash /usr/local/bin/ai_scripts/db/db_add "$2"

# Delete a new database or create a new user for an existing DB
elif [ "$1" = "--DELETE" ]; then
sudo bash /usr/local/bin/ai_scripts/db/db_del "$2" "$3"

# List DB
elif [ "$1" = "list" ]; then
sudo bash /usr/local/bin/ai_scripts/db/db_list

# List DB users
elif [ "$1" = "user" ] && [ "$2" = "list" ]; then
sudo bash /usr/local/bin/ai_scripts/db/user_list

# List DB users
elif [ "$1" = "user" ] && [ "$2" = "--DELETE" ]; then
sudo bash /usr/local/bin/ai_scripts/db/user_del "$3"

# List DB users
elif [ "$1" = "user" ] && [ "$2" = "--PASSWORD" ]; then
sudo bash /usr/local/bin/ai_scripts/db/user_del "$3"

# run backup script if installed
	elif [ "$1" = "backup" ]; then

	if [ -d "/usr/local/bin/ai_scripts/backup" ]; then

		bash /usr/local/bin/ai_scripts/backup/db_backup "$2"
fi

	elif [ "$1" = '--RESTORE' ]; then
	if [ -d "/usr/local/bin/ai_scripts/backup" ]; then
		bash /usr/local/bin/ai_scripts/backup/db_restore "$2"


		# else warning
		else
		echo "\033[32m
		It looks like the backup module is not installed..."

		echo "\033[0m"

		echo "  You can install it with 'ai backup --install'
		"
		fi
# show stop
		elif [ "$1" = "stop" ]; then
sudo service mysql stop

echo "\033[0m"

		echo "  MySQL service stopped'
		"

# show start
		elif [ "$1" = "start" ]; then
sudo service mysql start

echo "\033[0m"

		echo "  MySQL service started'
		"

# show restart
		elif [ "$1" = "restart" ]; then
sudo service mysql restart

echo "\033[0m"

		echo "  MySQL service restarted
		"

# show status
		elif [ "$1" = "status" ]; then
sudo service mysql status



fi

#todo add check for .my.cnf file
#todo error for saving folder on stand-alone install
#todo change screen output for cut and paste into lastpass
#todo need help file for aidb stand-alone