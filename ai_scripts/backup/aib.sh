#!/bin/bash



	##### Uninstall ######################################################
	if [ "$2" = '--UNINSTALL' ]; then
	rm -rf /usr/local/bin/ai_scripts/backup

echo "
Sucessfully uninstalled. The backups have not been deleted.
They are stored in the 'ai' folder in your home directory
"

	##### htdocs backup ######################################################
	elif [ "$1" = 'site' ] && [ "$2" = 'backup' ]; then

	bash /usr/local/bin/ai_scripts/backup/htdocs_backup "$3"



	##### htdocs restore ######################################################
	elif [ "$1" = 'site' ] && [ "$2" = 'restore' ]; then

	bash /usr/local/bin/ai_scripts/backup/htdocs_restore "$3"

	##### htdocs backup ######################################################
	elif [ "$1" = 'db' ] && [ "$2" = 'backup' ]; then

	bash /usr/local/bin/ai_scripts/backup/db_backup "$3"



	##### htdocs restore ######################################################
	elif [ "$1" = 'db' ] && [ "$2" = 'restore' ]; then

	bash /usr/local/bin/ai_scripts/backup/db_restore "$3"

fi

#todo create errors
#todo maybe put backups in a folder named after the site

#todo Confirm overwrite of existing backup
