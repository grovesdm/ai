#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

# Skip apt-get update if its not a fresh install
if [ "$1" = '--update' ]; then

ok "
  Doing and update...
" &&

echo "Wont be too long...
"

dpkg -s zip unzip 2>/dev/null >/dev/null || sudo apt-get -y zip unzip

#todo I might need to reconsider this.

else

# If first time upgrade everything

warn "
Doing and fresh install of the ai backup script...
" &&

echo "I will update dependencies... hold on...
"

sudo apt-get update && apt-get install zip unzip -y

fi

# Make the scripts directory if it doesn't exist (wont overwrite existing files)
mkdir -p /usr/local/bin/ai_scripts && mkdir -p /usr/local/bin/ai_scripts/backup

# Download and install latest version of aib
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/aib.sh -O /usr/local/bin/ai_scripts/backup/aib
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/db_backup.sh -O /usr/local/bin/ai_scripts/backup/db_backup
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/db_restore.sh -O /usr/local/bin/ai_scripts/backup/db_restore
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/htdocs_backup.sh -O /usr/local/bin/ai_scripts/backup/htdocs_backup
wget https://gitlab.com/grovesdm/ai/raw/master/ai_scripts/backup/htdocs_restore.sh -O /usr/local/bin/ai_scripts/backup/htdocs_restore

# Make the backup directory if it doesnt already exist wont overwrite if it does exist
mkdir -p $AI_FOLDER && mkdir -p $BACKUP_FOLDER && mkdir -p $SITE_BACKUP_FOLDER && mkdir -p $DB_BACKUP_FOLDER

clear &&

ok "
Installation of the backup script has completed...
" &&

echo "Ok thats done.... What now? Type ai for suggestions...
"

# Delete download from home directory (if exists)
rm -f ~/aibi
