#!/bin/bash

# Load config file

source /usr/local/bin/ai_scripts/ai.conf

# Check backup exists
if [ -f "$DB_BACKUP_FOLDER/$DB_BACKUP_NAME" ]; then

mysql -e "FLUSH TABLES WITH READ LOCK; SET GLOBAL read_only = ON;" &&

#mysql $1 < $DB_BACKUP_FOLDER$DB_BACKUP_NAME &&
mysql $TLD < $DB_BACKUP_FOLDER/$DB_BACKUP_NAME &&

mysql -e "SET GLOBAL read_only = OFF; UNLOCK TABLES;"

echo "
Database $DB_BACKUP_NAME has been restored ...


"



		# else warning
		else
		echo -e "\033[32m
		It looks like the backup $DB_BACKUP_NAME doesn't exist..."

		echo -e "\033[0m"

		echo "  You need to type the name exactly as it is called without the .sql

		"
		fi
