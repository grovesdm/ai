#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

# Install dependencies if required
dpkg -s vsftpd libpam-pwdfile makepasswd 2>/dev/null >/dev/null || sudo apt-get -y install vsftpd libpam-pwdfile makepasswd

# Create directories (non destructive)
#mkdir -p $AI_FOLDER/ftp
mkdir -p /usr/local/bin/ai_scripts/ftp
#mkdir -p $PASSWORDS_FOLDER
#mkdir -p $FTP_PASSWORDS_FOLDER

# Download files
wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/user-add.sh -O /usr/local/bin/ai_scripts/ftp/user-add
wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/user-delete.sh -O /usr/local/bin/ai_scripts/ftp/user-delete
wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/user-list.sh -O /usr/local/bin/ai_scripts/ftp/user-list
wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/tools.sh -O /usr/local/bin/ai_scripts/ftp/tools

# Backup vsftpd.conf
sudo mv /etc/vsftpd.conf /etc/vsftpd.conf.bak

# Then wget new one with new config
wget gitlab.com/grovesdm/ai/raw/master/ai_scripts/ftp/vsftpd.conf -O /etc/vsftpd.conf

# Register virtual users
# make a directory where you can put the password file which will be created when the first user is created.
sudo mkdir -p /etc/vsftpd

#create file #todo need to check this doesnt overwrite a populated file
sudo touch /etc/vsftpd/ftpd.passwd

#Configure PAM
#Backup original config
sudo mv /etc/pam.d/vsftpd /etc/pam.d/vsftpd.bak

#Create a new one with config
echo "
auth required pam_pwdfile.so pwdfile /etc/vsftpd/ftpd.passwd
account required pam_permit.so

" > /etc/pam.d/vsftpd

# Create a Local User without Shell Access
sudo userdel vsftpd # DELETE IF THEY EXIST
sudo rm -rf /home/vsftpd
sudo useradd --home /home/vsftpd --gid www-data -m --shell /bin/false vsftpd

# Restart vsftpd
sudo service vsftpd restart

#Enable this module to make FTP work.
sudo bash /usr/local/bin/ai_scripts/ftp/tools

clear &&

ok "\n  Installation has completed... \n"

ok "  Ok (vs)FTP is installed.... \n"

rm ~/aiftpi

