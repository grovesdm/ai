#!/bin/bash

source /usr/local/bin/ai_scripts/ai.conf

sudo sudo htpasswd -D /etc/vsftpd/ftpd.passwd $TLD

#Delete password file
rm -f $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD-ftp.txt

warn "\n User $TLD has been deleted so they can no longer access via ftp! \n"