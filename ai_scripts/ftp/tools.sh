#!/bin/bash
modprobe -i ip_conntrack_ftp

echo "
# This starts a special model to direct FTP connections to the correct port
modprobe -i ip_conntrack_ftp

" > /etc/network/if-pre-up.d/ai_ftp-rules

chmod +x /etc/network/if-pre-up.d/ai_ftp-rules


