#!/bin/bash

# LOAD GENERAL CONFIG
source /usr/local/bin/ai_scripts/ai.conf

# GENERATE 20 CHARACTER PASSWORD
PASS="$(makepasswd --chars 20)"

# CREATE USER WITH ABOVE PASSWORD
sudo htpasswd -db /etc/vsftpd/ftpd.passwd "$TLD" "$PASS"

ok "Created user $TLD with password $PASS..."

# SAVE TO FILE
echo "$TLD : $PASS" > $WEBROOT/$TLD/$SITEVHOSTDIR/$TLD-ftp.txt