Automated Installer Script Engine
=================================

**Status:** `BETA` - Not suitable for live environment

This is a personalised set of scripts I regularly user to complete server maintenance functions.

It is a collection of stuff I stole from the internets confirming the philosophy that [*'Everything is a remix'*](https://youtu.be/coGpmA4saEk)

This has been created for my own personal use `specifically for Debian 8.5 x64 (jessie) with Apache 2.4`.
 
You are free to use in any way but there is **no warranty whatsoever**.

---

Install
============

```
wget -qO ~/aii gitlab.com/grovesdm/ai/raw/master/ai.sh && bash ~/aii
```

Adds the following files and directories:
-----------------------------------------
```bash	
/usr/local/bin/ai
/usr/local/bin/ai_scripts/
/etc/ai/
/var/www/example.com/config
/var/www/example.com/logs
```

DB Script
=========
For a stand-alone DB server you can install the DB Module seperately

```
wget -qO ~/aidbi gitlab.com/grovesdm/ai/raw/master/ai_scripts/db/install.sh && bash ~/aidbi --install
```
Adds the following files and directories:
-----------------------------------------
```bash	
/usr/local/bin/aidb
/usr/local/bin/ai_scripts/
/usr/local/bin/ai_scripts/ai.conf
/etc/ai/backups/
```

`Note: MySQL password needs to be saved in the ~/.my.cnf file like so:`

```
[mysql]  
user=root  
password="password"
  
[mysqldump]  
user=root  
password="password"  

[mysqlshow]
user=root
password="password"
```
---

Firewall Manager
=================

To install standalone so you can manage just the firewall

```
wget -qO ~/aifi gitlab.com/grovesdm/ai/raw/master/ai_scripts/firewall/install.sh && bash ~/aifi --install
```

Uninstall
---------
*ai uninstall all ---
(deletes script and its files DOES NOT DELETE SITES or /etc/ai directory)

---

Goals
--------
* Easy to use
* Maintained
* Easy update
* Modular - backup, db, firewall and ftp can all be added as required
* Easy and clean un-installation

Usage
-----

Read the [help file](https://gitlab.com/grovesdm/ai/blob/master/ai_scripts/help.md) for usage instructions.


Notes on SSL vHost config
============
bundle needs to be in site root and named ssl-bundle.crt
key needs to be be named ssl.key


---
Repository URL: <https://gitlab.com/grovesdm/ai>